ROOT_PATH="$( cd "$BATS_TEST_DIRNAME" && cd .. && pwd )"
TEST_DATA="$ROOT_PATH/tests/data"
scroll="$ROOT_PATH/bin/scroll"

setup () {
    cd "$TEST_DATA/basic_ws"
}

@test "help on errors" {
    # Ensure execution without args contains the word "Use" or "Usage", and
    # "scroll"
    run $scroll
    [ "$status" -eq 1 ]
    [[ "$output" == *"scroll"* ]]
    [[ "$output" == *"Us"* ]]

    # Ensure that non-existant files have 'Could not find'
    run $scroll nonexistant
    [ "$status" -eq 1 ]
    [[ "$output" == *"Could not find nonexistant"* ]]

    # And that absolute gets path
    run $scroll ./something/nonexistant
    [ "$status" -eq 1 ]
    [[ "$output" == *"path:something/nonexistant"* ]]
}
