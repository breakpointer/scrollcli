# scrollcli

[![Build Status](https://drone.io/bitbucket.org/michaelb/scrollcli/status.png)](https://drone.io/bitbucket.org/michaelb/scrollcli/latest)

-------------------------

**Not much to see here.**

* Graphical version: http://bitbucket.org/michaelb/scrolleditor/

This is the CLI front-end for the very immature Scroll Editor, a
work-in-progress free software document processing system.

*Scroll is very WIP and not ready for public usage. If you really, really want
to dig in keep on reading.*

## Getting set up

To get a basic demo running, follow the following steps:

0. Check out this repo somewhere

1. First, install a recent version of `node.js`, along with a recent version of
npm.
    - The supported version is: `5.7` (same as ran by test server)
    - You might consider using `nvm` to easily install a version that is appropriate
        for your OS and architecture https://github.com/creationix/nvm
    - If you used `nvm`, remember to do `nvm use 5` to activate before continuing

2. Install dependencies by running `npm install -d`
3. Optionally, double check there's nothing wrong by running the unit tests
with `npm test`
4. Now, `git clone` the example workspace in some location, such as `scrollexamplews`

    * The example workspace is here: http://bitbucket.org/michaelb/scrollexamplews

## Playing around with very-WIP commandline interface

Now you can play around with the following commands (this assumes the example
workspace is at the top level):

To render the example document:

```
./bin/scroll ./scrollexamplews/document/document.md render
```

To see what actions are available for the sample document, and some
miscelaneous info:

```
./bin/scroll ./scrollexamplews/document/document.md
```

To customize, take a look at example tags in `scrollexamplews/tag`. The
`markdown=` under the `[markdown]` header allows you to specify custom syntax
for existing or custom tags. New documents placed in `document/` and new tags
in `tag/` will be picked up automatically. Additional styles for tags can be
created with different "targets" (example values might include "print" or
"web"), and then accessed with

```
./bin/scroll ./scrollexamplews/document/document.md render custom_target
./bin/scroll ./scrollexamplews/document/document.md rendercss custom_target
```

